# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic
Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-08-26
### Added
- `get-meta` can now be called with an `IO::Path` as the path.
- `put-meta` can now be called with positional arguments.
- `put-meta` can now be called with an `IO::Path` as the path.

### Changed
- The following dependencies have been removed because they are not being used:
  - `Config`
  - `File::Directory::Tree`
  - `File::Which`
  - `MIME::Base64`
  - `SemVer`

## [0.21.2] - 2018-08-25
### Added
- Add `source-url` to default `META6.json` hashes.

## [0.21.1] - 2018-08-25
### Changed
- Updated `readme.pod6` template to not escape the `<` and `>` used to
  encapsulate the author's email address.
- Updated the `gitlab-ci.yml` template to contain `--test-depends` and
  `--/test` for the `zef` installation step. This will ensure the test
  dependencies are included, and that the test phase will be skipped.

## [0.21.0] - 2018-07-10
### Added
- Add default Perl 6 Pod blocks to the end of new Perl 6 files.
- Add template for a CHANGELOG.
- Add `new-meta` sub to get a clean META6 hash, containing all supported keys.
- Add template for a README in Perl 6 Pod formatting.
- Add `*.yml` configuration to EditorConfig template.

### Changed
- `.editorconfig` now uses `utf-8` as character set name, instead of `utf8`.
- An `api` key has been added to the `META6.json` to assist in future breaking
  changes.
- The `module/test` template has been updated to contain a `skip-all` by
  default. This should let the gitlab-ci pass by default, instead of fail by
  default.
- The `.gitignore` template has been updated to remove breaking comments, and to
  include CommaIDE files to be ignored as well.
- Context names used in the GitLab CI template have changed to be in-line with
  the naming used in other templates.

## [0.20.0] - 2018-06-24
### Added
- Add support for GitLab CI configuration file templates

## [0.19.2] - 2018-03-16
### Changed
- Dependency versions are no longer locked to a single version ([GitHub#34](https://github.com/scriptkitties/perl6-dist-helper/issues/34))
