#! /usr/bin/env perl6

use v6.c;

use Dist::Helper::Template;
use File::Temp;
use Test;

plan 1;

my IO::Path $module-dir .= new: ".";

subtest "readme.pod6" => {
	plan 3;

	chdir tempdir;

	my %context =
		name => "Local::Test::Module",
		author => "Patrick 'tyil' Spek",
		version => "1.0.0",
		description => "Pull all this info from the META6.json in practice.",
		license => "AGPL-3.0",
	;

	my IO::Path $output .= new: "readme.pod6";

	ok template("readme.pod6", $output.absolute, :%context), "Template call is correct";
	ok $output.IO.e && $output.IO.f, "Output file gets created correctly";
	is $output.slurp.trim, $module-dir.add("t/files/readme.pod6").slurp.trim, "Output has correct contents";
}

# vim: ft=perl6 noet
