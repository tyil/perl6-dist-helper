#! /usr/bin/env perl6

use v6.c;

use Dist::Helper::Meta;
use Test;
use File::Temp;

plan 1;

subtest "get-meta" => {
	plan 2;

	subtest "Don't add missing keys" => {
		plan 2;

		my %meta = get-meta;

		is-deeply %meta<provides>, %(
			"Dist::Helper" => "lib/Dist/Helper.pm6",
			"Dist::Helper::Clean" => "lib/Dist/Helper/Clean.pm6",
			"Dist::Helper::Meta" => "lib/Dist/Helper/Meta.pm6",
			"Dist::Helper::Path" => "lib/Dist/Helper/Path.pm6",
			"Dist::Helper::Template" => "lib/Dist/Helper/Template.pm6",
		), "provides has correct value";

		is-deeply %meta<authors>, [
			"Patrick Spek <p.spek@tyil.work>",
		], "authors has the correct value";
	}

	subtest "Add missing keys" => {
		plan 5;

		chdir tempdir;

		put-meta(meta => %(
			perl => "6.c",
			version => "0.20.0",
			tags => [
				"devel",
				"ecosystem",
			],
		));

		my %meta = get-meta(:add-missing);

		ok %meta<api>:exists, "api key was merged in";
		ok %meta<description>:exists, "description key was merged in";

		is %meta<perl>, "6.c", "perl key kept correct value";
		is %meta<version>, "0.20.0", "version key kept correct value";

		is-deeply %meta<tags>, [
			"devel",
			"ecosystem",
		], "tags key kept correct value";
	}
}

# vim: ft=perl6 noet
