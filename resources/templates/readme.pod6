=begin pod

=NAME    {{ name }}
=AUTHOR  {{{ author }}}
=VERSION {{ version }}

=head1 Description

{{ description }}

=head1 Installation

Install this module through L<zef|https://github.com/ugexe/zef>:

=begin code :lang<sh>
zef install {{ name }}
=end code

=head1 License

This module is distributed under the terms of the {{ license }}.

=end pod
