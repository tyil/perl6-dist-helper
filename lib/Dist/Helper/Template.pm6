#! /usr/bin/env false

use v6.c;

use Template::Mustache;

unit module Dist::Helper::Template;

multi sub template (
	Str:D $template,
	:%context,
	--> Str
) is export {
	my Str $absolute = "templates/$template";
	my Distribution::Resource $resource = %?RESOURCES{$absolute};

	die "Resource '$absolute' does not exist" unless $resource;

	Template::Mustache.render($resource.slurp, %context);
}

multi sub template (
	Str:D $template,
	IO::Path:D $destination,
	:%context,
	Bool:D :$clobber = False
) is export {
	die "$destination already exists. Set :clobber to overwrite the existing file." if $destination.IO.e && !$clobber;

	mkdir $destination.parent.absolute;
	spurt($destination, samewith($template, :%context));
}

multi sub template (
	Str:D $template,
	Str:D $destination,
	:%context,
	Bool:D :$clobber = False
) is export {
	samewith($template, $destination.IO, :%context, :$clobber);
}
