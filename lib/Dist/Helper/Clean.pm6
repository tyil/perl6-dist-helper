#! /usr/bin/env false

use v6.c;

use Dist::Helper::Meta;

unit module Dist::Helper::Clean;

multi sub clean-files (
	IO::Path:D $path,
	Bool:D :$force = False,
	Bool:D :$verbose = False,
	--> Array
) is export {
	my %meta = get-meta($path);
	my @provides = %meta<provides>.values.map({ $path.add($_) });
	my @resources = %meta<resources>.map({ $path.add($_); });
	my @orphans;

	# Clean up bin and lib directories
	for < bin lib > -> $directory {
		for find-files($path.add($directory)) -> $file {
			next if $file.absolute ~~ /\.precomp/;
			next if @provides ∋ $file.absolute;

			@orphans.push: $file;
		}
	}

	# Clean up resources
	for find-files($path.add("resources")) -> $file {
		next if @resources ∋ $file.subst("resources/", "");

		@orphans.push: $file;
	}

	@orphans;
}

multi sub clean-files (
	Str:D $path = ".",
	Bool:D :$force = False,
	Bool:D :$verbose = False,
	--> Array
) is export {
	samewith($path.IO, :$force, :$verbose)
}

multi sub clean-meta (
	IO::Path:D $path,
	Bool:D :$force = False,
	Bool:D :$verbose = False,
	--> Hash
) is export {
	my %meta = get-meta($path);
	my %provides;
	my @resources;

	# Clean up provides
	for %meta<provides>.kv -> $key, $value {
		if ($path.add($value).e) {
			%provides{$key} = $value;

			next;
		}

		say "Removing provides.$key ($value)" if $verbose;
	}

	# Clean up resources
	for %meta<resources>.values -> $value {
		if ($path.add("resources").add($value).e) {
			@resources.push: $value;

			next
		}

		say "Removing resources.$value" if $verbose;
	}

	%meta<provides> = %provides;
	%meta<resources> = @resources;

	%meta;
}

multi sub clean-meta (
	Str:D $path = ".",
	Bool:D :$force = False,
	Bool:D :$verbose = False,
	--> Hash
) is export {
	samewith($path.IO, :$force, :$verbose)
}

multi sub find-files (
	Str:D $path
	--> List
) is export {
	find-files($path.IO)
}

multi sub find-files (
	IO::Path:D $path
	--> List
) is export {
	my @files;

	for $path.dir -> $object {
		if ($object.IO.d) {
			@files.append: find-files($object);

			next;
		}

		@files.append: $object;
	}

	@files;
}

# vim: ft=perl6 noet
