#! /usr/bin/env false

use v6.c;

use Hash::Merge;
use JSON::Fast;

unit module Dist::Helper::Meta;

#| Read a dist's C<META6.json> file, and return it interpreted as a hash. If
#| C<:$add-missing> is set, it well be merged into a complete, empty META6 hash,
#| ensuring there are no missing keys.
multi sub get-meta (
	IO::Path:D $path,
	Bool:D :$add-missing = False,
	--> Hash
) is export {
	my IO::Path $meta6-file = $path.add("META6.json");

	if (!$meta6-file.e) {
		die "No META6.json in {$path.absolute}";
	}

	my %meta = from-json(slurp($meta6-file.absolute));

	return merge-hash(new-meta, %meta, :no-append-array) if $add-missing;

	%meta;
}

multi sub get-meta (
	Str:D $path = ".",
	Bool:D :$add-missing = False,
	--> Hash
) is export {
	samewith($path.IO, :$add-missing);
}

#| Get the base structure of a new META6 hash. This can then be used to create a
#| new C<META6.json> without missing keys.
sub new-meta (
	--> Hash
) is export {
	%(
		meta-version => 0,
		perl => "6.c",
		name => "",
		description => "",
		license => "",
		authors => [],
		tags => [],
		api => "*",
		version => "*",
		provides => %(),
		depends => [],
		resources => [],
		source-url => "",
	);
}

#| Save a dist's META6 hash to C<META6.json> in the given C<:$path>.
multi sub put-meta (
	:%meta!,
	IO::Path:D :$path,
	:$clobber = True
) is export {
	my IO::Path $meta6 = $path.add("META6.json");

	die "Not clobbering {$meta6.absolute}" if $meta6.e && !$clobber;

	# Manually sort arrays used in META6
	my @sortable-arrays = (
		"depends",
		"resources",
		"tags",
	);

	for @sortable-arrays -> $array {
		next if %meta{$array}:!exists;
		next if %meta{$array}.elems < 1;

		%meta{$array} = %meta{$array}.sort;
	}

	spurt($meta6, to-json(%meta, :sorted-keys))
}

multi sub put-meta (
	:%meta,
	Str:D :$path = ".",
	:$clobber = True,
) is export {
	samewith(:%meta, :$clobber, path => $path.IO);
}

multi sub put-meta (
	%meta,
	IO::Path:D $path,
	:$clobber = True,
) is export {
	samewith(:%meta, :$path, :$clobber);
}

multi sub put-meta (
	%meta,
	Str:D $path = ".",
	:$clobber = True,
) is export {
	samewith(:%meta, :$clobber, path => $path.IO);
}
